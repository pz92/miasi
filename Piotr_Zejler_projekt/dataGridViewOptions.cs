﻿using System;
using System.Windows.Forms;
using System.Drawing;
using MaterialSkin.Controls;
using System.Collections.Generic;

namespace Piotr_Zejler_projekt
{
    class dataGridViewOptions
    {
        //dodawanie nazw kolumn do dataGridView na podstawie listView
        public void addColumnsToDataGridViewFromListView(DataGridView dgv, MaterialListView listView)
        {
            for (int i = 0; i < listView.Items.Count; i++)
            {
                if (checkDgvContainsHeader(dgv, listView.Items[i].Text, "column") == false)
                {
                    addColumntoDataGridView(dgv, listView.Items[i].Text);
                }
            }
            dgv.ColumnHeadersHeight = 70;

        }

        //dodaje kolumnę do DGV z wybranym headerem
        public void addColumntoDataGridView(DataGridView dgv, string header)
        {
            dgv.Columns.Add(new DataGridViewColumn() { HeaderText = header, CellTemplate = new DataGridViewTextBoxCell() });
            dgv.Columns[dgv.ColumnCount-1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
            dgv.Columns[dgv.ColumnCount-1].HeaderCell.Style.Font = new Font("Arial", 13F, FontStyle.Bold, GraphicsUnit.Pixel);
        }

        //dodawanie nazw rzędów do dataGridView na podstawie listView
        public void addRowsToDataGridViewFromListView(DataGridView dgv, MaterialListView listView)
        {
            for (int i = 0; i < listView.Items.Count; i++)
            {
                if(checkDgvContainsHeader(dgv, listView.Items[i].Text, "row") == false)
                {
                    dgv.Rows.Add("");
                    dgv.Rows[i].HeaderCell.Value = listView.Items[i].Text;

                    dgv.Rows[i].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgv.Rows[i].HeaderCell.Style.Font = new Font("Arial", 13F, FontStyle.Bold, GraphicsUnit.Pixel);

                    dgv.Rows[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    dgv.Rows[i].DefaultCellStyle.Font = new Font("Arial", 15F, FontStyle.Regular, GraphicsUnit.Pixel);

                    dgv.Rows[i].Height = 40;
                }
            }

            dgv.RowHeadersWidth = 250;
        }

        //zwraca true jeśli dgv zawiera header podanych jako text, false jeśli nie ma
        public bool checkDgvContainsHeader(DataGridView dgv, string text, string header)
        {
            int check = 0;
            if(header == "row")
            {
                foreach (DataGridViewRow row in dgv.Rows)
                {
                    if (row.HeaderCell.Value.ToString() == text)
                    {
                        check++;
                    }
                }
            }
            else if(header == "column")
            {
                foreach (DataGridViewColumn col in dgv.Columns)
                {
                    if (col.HeaderCell.Value.ToString() == text)
                    {
                        check++;
                    }
                }
            }
            
            if(check == 0)
                return false;

            return true;
        }

        public void deleteDgvColumn(DataGridView dgv, string colHeader)
        {
            if(dgv.Columns[dgv.ColumnCount-1].HeaderText == colHeader)
            {
                dgv.Columns.RemoveAt(dgv.ColumnCount - 1);
            }
        }

        public void dataGridView_Paint(DataGridView dgv, PaintEventArgs e)
        {
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            Font fontRegular = new Font("Arial", 13F, FontStyle.Regular, GraphicsUnit.Pixel);
            Font fontBold = new Font("Arial", 13F, FontStyle.Bold, GraphicsUnit.Pixel);

            Rectangle r1 = dgv.GetCellDisplayRectangle(-1, -1, true);
            r1.X += 1;
            r1.Y += 1;
            r1.Width = 250;
            r1.Height = r1.Height / 2 - 2;
            e.Graphics.FillRectangle(new SolidBrush(Color.Transparent), r1);
            e.Graphics.DrawString("Decyzje", fontRegular, new SolidBrush(dgv.ColumnHeadersDefaultCellStyle.ForeColor), r1, format);

            r1 = dgv.GetCellDisplayRectangle(-1, -1, true);
            r1.X += 1;
            r1.Y += r1.Height / 2 + 14;
            r1.Width = 250;
            r1.Height = r1.Height / 2 - 15;
            e.Graphics.FillRectangle(new SolidBrush(Color.Transparent), r1);
            e.Graphics.DrawString("Zamówienie", fontBold, new SolidBrush(dgv.ColumnHeadersDefaultCellStyle.ForeColor), r1, format);


            r1 = dgv.GetCellDisplayRectangle(0, -1, true);
            r1.X += 1;
            r1.Y += 1;

            int width = 0;
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                width += dgv.GetCellDisplayRectangle(i, -1, true).Width;
            }
            r1.Width = width - r1.Width;
            r1.Height = r1.Height / 2 - 2;
            e.Graphics.FillRectangle(new SolidBrush(Color.White), r1);

            r1 = dgv.GetCellDisplayRectangle(0, -1, true);
            r1.X += 1;
            r1.Y += 1;

            r1.Width = 200;
            r1.Height = r1.Height / 2 - 2;
            e.Graphics.FillRectangle(new SolidBrush(Color.White), r1);
            e.Graphics.DrawString("Stany natury", fontRegular, new SolidBrush(dgv.ColumnHeadersDefaultCellStyle.ForeColor), r1, format);
        }

        //przywracanie białego koloru wszystkim komórkom
        public void clearCellColor(DataGridView dgv)
        {
            DataGridViewCellStyle rowStyle;

            for (int j = 0; j < dgv.RowCount; j++) //wiersze
            {
                for (int i = 0; i < dgv.ColumnCount; i++) //kolumny
                {
                    dgv.Rows[j].Cells[i].Style.BackColor = Color.White;
                }

                rowStyle = dgv.Rows[j].HeaderCell.Style;
                rowStyle.BackColor = Color.White;
                dgv.Rows[j].HeaderCell.Style = rowStyle;
            }
        }

        //zmiana koloru komórki wg indeksu rzędu i kolumny, dodatkowo jeśli w komórce w tym samym rzędzie jest taka sama wartość to jej kolor też zostanie zmieniony
        public void changeCellColor(DataGridView dgv, double[,] array, int rowIndex, int columnIndex)
        {
            //clearCellColor(dgv);
            dgv.ClearSelection();
            for (int i = 0; i < array.GetLength(1); i++)
            {
                if (array[rowIndex, columnIndex] == array[rowIndex, i])
                {
                    dgv.Rows[rowIndex].Cells[i].Style.BackColor = Color.LightGreen;
                }
            }

        }

        //zmiana koloru komórki w której jest konkretna wartość
        public void changeCellColor(DataGridView dgv, double value)
        {
           //clearCellColor(dgv);
            dgv.ClearSelection();
            for (int i = 0; i < dgv.ColumnCount; i++) //kolumny
            {
                for (int j = 0; j < dgv.RowCount; j++) //wiersze
                {
                    if (Int32.Parse(dgv.Rows[j].Cells[i].Value.ToString()) == value)
                    {
                        dgv.Rows[j].Cells[i].Style.BackColor = Color.LightGreen;

                    }
                }
            }
        }

        //zmiana koloru headera wiersza
        public void changeHeaderColor(DataGridView dgv, int row)
        {
            //clearCellColor(dgv);
            dgv.ClearSelection();

            DataGridViewCellStyle rowStyle;
            rowStyle = dgv.Rows[row].HeaderCell.Style;
            rowStyle.BackColor = Color.LightGreen;
            dgv.Rows[row].HeaderCell.Style = rowStyle;

        }

        public bool isEmptyDgv(DataGridView dgv)
        {
            int emptyCells = 0;
            for (int i = 0; i < dgv.ColumnCount; i++) //kolumny
            {
                for (int j = 0; j < dgv.RowCount; j++) //wiersze
                {
                    if (dgv.Rows[j].Cells[i].Value == null || dgv.Rows[j].Cells[i].Value.ToString() == "")
                    {
                        emptyCells++;                 
                    }
                }
            }

            if (emptyCells != 0)
                return true;
            else
                return false;
        }

        public void resetRowHeader(DataGridView dgv)
        {
            for (int i = 0; i < dgv.RowCount; i++)
            {
                if(dgv.Rows[i].HeaderCell.Value.ToString().IndexOf('\n') >= 0)
                {
                    string[] header = dgv.Rows[i].HeaderCell.Value.ToString().Split('\n');
                    dgv.Rows[i].HeaderCell.Value = header[0];
                }
            }
        }

        public void addTextToRowHeader(DataGridView dgv, int dominatedIndex, string text, List<double> valueList)
        {
            Console.WriteLine(dominatedIndex + "<<<");
            for (int i = 0, j=0 ; i < dgv.RowCount; i++)
            {
                if(i == dominatedIndex)
                {
                    string[] header = dgv.Rows[i].HeaderCell.Value.ToString().Split('\n');
                    dgv.Rows[i].HeaderCell.Value = header[0];
                }
                else
                {
                    string[] header = dgv.Rows[i].HeaderCell.Value.ToString().Split('\n');
                    dgv.Rows[i].HeaderCell.Value = header[0] + "\n" + text + ": " + valueList[j];
                    j++;
                }
            }
        }
    }
}
