﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Piotr_Zejler_projekt
{
    class Criteria
    {
        public double kryteriumHurwicza(double [,]array)
        {
            double maxValue = array[0, 0];

            for (int i = 0; i < array.GetLength(1); i++) //kolumny
            {
                for (int j = 0; j < array.GetLength(0); j++) //wiersze
                {
                    if (maxValue < array[j, i])
                    {
                        maxValue = array[j, i];
  
                    }
                }
            }

            return maxValue;
        }

        public Tuple<int, int> kryteriumWalda(double[,] array)
        {

            double minValue;
            double maxValue;
            int rowIndex = 0;
            int columnIndex = 0;
            List<double> listMinValues = new List<double>();
            List<int> listRowIndexes = new List<int>();
            List<int> listColumnIndexes = new List<int>();

            for (int j = 0; j < array.GetLength(0); j++) //wiersze
            {
                minValue = array[j, 0];
                
                for (int i = 0; i < array.GetLength(1); i++) //kolumny
                {
                    if (minValue >= array[j, i])
                    {
                        minValue = array[j, i];
                        rowIndex = j;
                        columnIndex = i;
                    }
                }
                listMinValues.Add(minValue);
                listRowIndexes.Add(rowIndex);
                listColumnIndexes.Add(columnIndex);
            }

            maxValue = listMinValues.ElementAt(0);
            rowIndex = listRowIndexes.ElementAt(0);
            columnIndex = listColumnIndexes.ElementAt(0);

            for (int i=1; i< listMinValues.Count; i++)
            {
                if (listMinValues[i] > maxValue)
                {
                    maxValue = listMinValues[i];
                    rowIndex = listRowIndexes[i];
                    columnIndex = listColumnIndexes[i];
                }
            }

            var result = Tuple.Create(rowIndex, columnIndex);   
            return result;
        }

        public Tuple<int, int> kryteriumSavagea(double[,] array)
        {
            double minValue;
            double maxValue;
            int rowIndex = 0;
            int columnIndex = 0;
            List<double> listMaxValues = new List<double>();
            List<int> listRowIndexes = new List<int>();
            List<int> listColumnIndexes = new List<int>();

            for (int j = 0; j < array.GetLength(0); j++) //wiersze
            {
                maxValue = array[j, 0];

                for (int i = 0; i < array.GetLength(1); i++) //kolumny
                {
                    if (maxValue <= array[j, i])
                    {
                        maxValue = array[j, i];
                        rowIndex = j;
                        columnIndex = i;
                    }
                }
                listMaxValues.Add(maxValue);
                listRowIndexes.Add(rowIndex);
                listColumnIndexes.Add(columnIndex);
            }

            minValue = listMaxValues.ElementAt(0);
            rowIndex = listRowIndexes.ElementAt(0);
            columnIndex = listColumnIndexes.ElementAt(0);

            for (int i = 1; i < listMaxValues.Count; i++)
            {
                if (minValue > listMaxValues[i])
                {
                    minValue = listMaxValues[i];
                    rowIndex = listRowIndexes[i];
                    columnIndex = listColumnIndexes[i];
                }
            }

            var result = Tuple.Create(rowIndex, columnIndex);
            return result;
        }


        public Tuple<double, int, List<double>> kryteriumLaplaceaOW(double[,] array, int [] arrayProbabilities, bool probabilities = false)
        {
            List<double> listOW = new List<double>();
            double OW = 0;

            if (probabilities)
            {
                for (int j = 0; j < array.GetLength(0); j++) //wiersze
                {
                    for (int i = 0; i < array.GetLength(1); i++) //kolumny
                    {
                        OW += array[j, i] * (double)arrayProbabilities[i] / (double)100;
                    }
                    listOW.Add(Math.Round(OW,2));
                    OW = 0;
                }           
            }
            else
            {
                for (int j = 0; j < array.GetLength(0); j++) //wiersze
                {
                    for (int i = 0; i < array.GetLength(1); i++) //kolumny
                    {
                        OW += array[j, i] * (double)1 / (double)arrayProbabilities.Length;  //domyślne prawdopodobieństwo takie samo dla wszystkich stanów
                    }
                    listOW.Add(Math.Round(OW, 2));
                    OW = 0;
                }
            }

            double maxValue = listOW[0];
            int maxValueIndex = 0;
            for(int i=1; i<listOW.Count; i++)
            {
                if(maxValue < listOW[i])
                {
                    maxValue = listOW[i];
                    maxValueIndex = i;
                }
            }

            var result = Tuple.Create(maxValue, maxValueIndex, listOW);
            return result;
        }

        public Tuple<int, List<double>> kryteriumOSM(double[,] array, int[] arrayProbabilities)
        {
            List<double> listOSM = new List<double>();
            double OSM = 0;

            for (int j = 0; j < array.GetLength(0); j++) //wiersze
            {
                for (int i = 0; i < array.GetLength(1); i++) //kolumny
                {
                    OSM += array[j, i] * (double)arrayProbabilities[i] / (double)100;
                }
                listOSM.Add(Math.Round(OSM,2));
                OSM = 0;
            }
            
            double minValue = listOSM[0];
            int minValueIndex = 0;
            for (int i = 1; i < listOSM.Count; i++)
            {
                if (minValue > listOSM[i])
                {
                    minValue = listOSM[i];
                    minValueIndex = i;
                }
            }

            var result = Tuple.Create(minValueIndex, listOSM);
            return result;

        }

        public double kryteriumOWDI(double[,] array, int[] arrayProbabilities)
        {
            double OWDI = 0;
            double maxValue;
            for (int i = 0; i < array.GetLength(1); i++) //kolumny
            {
                maxValue = array[0, i];
                for (int j = 0; j < array.GetLength(0); j++) //wiersze
                {
                    if (maxValue < array[j, i])
                    {
                        maxValue = array[j, i];
                    }

                }
                OWDI += maxValue * (double)arrayProbabilities[i] / (double)100;
            }
            return OWDI;
        }

        public Tuple<double, double> kryteriumODI(double[,] array, int[] arrayProbabilities)
        {
            var OW = kryteriumLaplaceaOW(array, arrayProbabilities, true);
            double OWDI = kryteriumOWDI(array, arrayProbabilities);
            double ODI = OWDI - OW.Item1;

            var result = Tuple.Create(Math.Round(OWDI, 2), Math.Round(ODI, 2));
            return result;
        }


    }
}
