﻿using System.Drawing;
using System.Windows.Forms;


namespace Piotr_Zejler_projekt
{
    public class ControlSetup
    {

        public void centerControl(Control control)
        {
            control.Location = new Point((int)((control.Parent.Width / 2) - control.Width / 2), control.Location.Y);
        }

        public void leftControl(Control control)
        {
            control.Location = new Point((int)((control.Parent.Width / 2) - control.Width), control.Location.Y);
        }

        public void rightControl(Control control)
        {
            control.Location = new Point((int)((control.Parent.Width / 2)), control.Location.Y);
        }

        public void rightControlMax(Control control)
        {
            control.Location = new Point((int)(control.Parent.Width - control.Width), control.Location.Y);
        }

        public void disableAutoSize(Control control)
        {
            control.AutoSize = false;
        }

        public void sizeControl(Control control, string width, int height = 40)
        {
            disableAutoSize(control);
            control.Height = height;

            if (width == "sml")
            {
                control.Width = 200;
            }
            else if (width == "mid")
            {
                control.Width = 400;
            }
            else if (width == "big")
            {
                control.Width = 600;
            }

        }

        public void visibleControl(Control control, bool state)
        {
            control.Visible = state;
        }

        public void enableControl(Control control, bool state)
        {
            control.Enabled = state;
        }
    }
}
