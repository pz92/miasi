﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using System.Collections.Generic;
using System.Linq;

namespace Piotr_Zejler_projekt
{
    public partial class Form1 : MaterialForm
    {
        private MaterialSingleLineTextField textBox;
        private MaterialListView listView;
        private MaterialRaisedButton btnDodaj;
        private Control btnWyczysc;
        private Control btnUsun;
        private Criteria criteria = new Criteria();
        private ControlSetup cs = new ControlSetup();
        private dataGridViewOptions dgvo = new dataGridViewOptions();
        private double[,] arrSalary;
        private double[,] arrLosses;
        private int[] arrProbabilities;
        private Label label;
        private TrackBar trackBar;
        private string header;
        private int dominatedIndex;

        public Form1()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.LightBlue800, Primary.LightBlue900, Primary.LightBlue500, Accent.LightBlue200, TextShade.WHITE);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            controlsSetup();
            tabControlManager();
            labelHint.Hide();

        }
        

        private void addToList(MaterialSingleLineTextField textBox, MaterialListView listView)
        {
            if (!string.IsNullOrEmpty(textBox.Text) && !string.IsNullOrWhiteSpace(textBox.Text))
            {
                ListViewItem item = listView.FindItemWithText(textBox.Text);
                if (item == null)
                {
                    listView.Items.Add(textBox.Text);
                    textBox.Clear();
                    if (listView.Items.Count >= 2)
                    {
                        cs.enableControl(btnDalej, true);
                    }
                }
                dataGridViewTablicaStrat.Rows.Clear();
                dataGridViewTablicaStrat.Columns.Clear();
            }
        }

        private void removeFromList(MaterialListView listView, string header)
        {
            if (listView.SelectedItems.Count > 0)
            {
                int selected = listView.SelectedItems[0].Index;
                listView.SelectedItems[0].Remove();

                if(header == "row")
                    if(dataGridViewTablicaWyplat.Rows.Count > 0)
                        dataGridViewTablicaWyplat.Rows.RemoveAt(selected);
                else if(header == "column")
                    if (dataGridViewTablicaWyplat.Columns.Count > 0)
                        dataGridViewTablicaWyplat.Columns.RemoveAt(selected);

                dataGridViewTablicaStrat.Rows.Clear();
                dataGridViewTablicaStrat.Columns.Clear();
            
                if (listView.Items.Count < 2)
                {
                    cs.enableControl(btnDalej, false);
                }
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            addToList(textBox, listView);
        }

        private void btnWyczysc_Click(object sender, EventArgs e)
        {
            if(listView.Items.Count > 0)
            {
                listView.Items.Clear();
                dataGridViewTablicaWyplat.Rows.Clear();
                dataGridViewTablicaWyplat.Columns.Clear();
                dataGridViewTablicaStrat.Rows.Clear();
                dataGridViewTablicaStrat.Columns.Clear();
            }
        }

        private void btnUsun_Click(object sender, EventArgs e)
        {
            removeFromList(listView, header);
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                addToList(textBox, listView);
            }
        }

        private void btnDalej_Click(object sender, EventArgs e)
        {
            tabControl.SelectedIndex++;
            tabControlManager();
        }

        private void btnWstecz_Click(object sender, EventArgs e)
        {
            tabControl.SelectedIndex--;
            tabControlManager();
        }

        private void dataGridView_Paint(object sender, PaintEventArgs e)
        {
            dgvo.dataGridView_Paint((DataGridView)sender, e);
        }

        //Sprawdzenie czy któraś decyzja jest zdominowana, czyli ma najmniejsze wartości w porównaniu do innych dla danych stanów
        private int isDominated(DataGridView dgv)
        {
            int theLowestIndex = 0;                             //indeks komórki w kolumnie która ma najmniejszą wartość
            int[] theLowestIndexes = new int[dgv.ColumnCount];  //tablica indeksów komórek z najmniejszą wartością, -1 jeśli w kolumnie powtarzają się takie same najmniejsze wartości
            bool theSameLowValue = false;                       //dla sprawdzenia czy w kolumnie powtarzają się takie same najmniejsze wartości

            for (int i = 0; i < dgv.ColumnCount; i++)
            {
                theLowestIndex = 0;
                theSameLowValue = false;
                for (int j = 0; j < dgv.RowCount; j++)
                {
                    if (Int32.Parse(dgv.Rows[j].Cells[i].Value.ToString()) < Int32.Parse(dgv.Rows[theLowestIndex].Cells[i].Value.ToString()))
                    {
                        theLowestIndex = j;
                    }
                }

                for (int j = 0; j < dgv.RowCount; j++)
                {
                    if (Int32.Parse(dgv.Rows[j].Cells[i].Value.ToString()) == Int32.Parse(dgv.Rows[theLowestIndex].Cells[i].Value.ToString()) && j != theLowestIndex)
                    {
                        theSameLowValue = true;
                        break;
                    }
                }

                if (!theSameLowValue)
                {         
                    theLowestIndexes[i] = theLowestIndex;
                }
                else
                {
                    theLowestIndexes[i] = -1;
                }      
            }

            return isTheSameIndexes(theLowestIndexes);
        }

        //do sprawdzenia czy w tablicy wszystkie wartości są takie same, jesli są te same to zwraca wartość zerowego indeksu jeśli nie to zwraca -1
        //używana do sprawdzenia zdominowanej decyzji
        private int isTheSameIndexes(int [] array)
        {
            int checkIndexes = 1;
            for (int i = 0; i < array.Length - 1; i++)
            {
                if (array[i] == array[i + 1])
                {
                    checkIndexes++;
                }
            }

            if(checkIndexes == array.Length)
            {
                return array[0];
            }
            else
            {
                return -1;
            }
            
        }

        private void prepareCalculation()
        {
            dgvo.deleteDgvColumn(dataGridViewTablicaWyplat, "OW");
            dgvo.clearCellColor(dataGridViewTablicaWyplat);
            dgvo.clearCellColor(dataGridViewTablicaStrat);
            arrSalary = dgvToArray(dataGridViewTablicaWyplat);
            calculateLosses(arrSalary);
            ArrayToDgv(arrLosses, dataGridViewTablicaStrat);
        }

        //przerzucenie wartości z dgv do tablicy z pominięciem wiersza zdominowanego
        private double[,] dgvToArray(DataGridView dgv)
        {
            dominatedIndex = isDominated(dataGridViewTablicaWyplat);
            double[,] array;

            if (dominatedIndex != -1)
            {
                array = new double[dgv.RowCount - 1, dgv.ColumnCount];
            }
            else
            {
                array = new double[dgv.RowCount, dgv.ColumnCount];
            }

            for (int i = 0, i2 = 0 ; i < dgv.RowCount; i++)
            {
                if (dominatedIndex != i)
                { 
                    for (int j = 0; j < dgv.ColumnCount; j++)
                        array[i2, j] = Int32.Parse(dgv.Rows[i].Cells[j].Value.ToString());

                    i2++;
                }
            }

            return array;
        }

        private void infoDominatedDecision(DataGridView dgv)
        {
            if (dominatedIndex != -1)
            {
                labelDecyzjeZdominowane.Text = "DEZYCJE ZDOMINOWANE: " + dgv.Rows[dominatedIndex].HeaderCell.Value.ToString();
            }
            else
            {
                labelDecyzjeZdominowane.Text = "DEZYCJE ZDOMINOWANE: BRAK";
            }
        }

        //przerzucenie wartości z dgv do tablicy z pominięciem wiersza zdominowanego
        private void ArrayToDgv(double [,] array, DataGridView dgv)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    dgv.Rows[i].Cells[j].Value = array[i, j].ToString();
                }
            }
        }

        //stworzenie sliderów do oceny prawdopodobieństwa dla wartości z listView
        private void createControls(ListView listView)
        {
            panelTrackBars.Controls.Clear();
            arrProbabilities = new int[listView.Items.Count];
            for(int i=0, y=1; i< listView.Items.Count; i++, y += 80)
            {
                label = new Label();
                trackBar = new TrackBar();

                panelTrackBars.Controls.Add(label);
                label.Font = new Font("Arial", 13F, FontStyle.Bold, GraphicsUnit.Pixel);
                label.TextAlign = ContentAlignment.MiddleCenter;
                cs.sizeControl(label, "mid");
                cs.centerControl(label);
                label.Location = new Point(label.Location.X, 10+y);
                label.Name = "labelTrackBar" + i;

                panelTrackBars.Controls.Add(trackBar);
                cs.sizeControl(trackBar, "mid");
                cs.centerControl(trackBar);
                trackBar.Location = new Point(trackBar.Location.X, 50+y);
                trackBar.BackColor = Color.White;
                trackBar.Minimum = 1;
                trackBar.Maximum = 100;
                trackBar.Value = (int) (100 / listView.Items.Count);

                trackBar.TickStyle = TickStyle.None;
                trackBar.TickFrequency = 1;
                trackBar.Name = "trackBar" + i;
                trackBar.Scroll += new System.EventHandler(this.trackBar_Scroll);

                label.Text = "Stan " + (i + 1) + ": " + listView.Items[i].Text + " (" + trackBar.Value +"%)";

                arrProbabilities[i] = 100/arrProbabilities.Length;
            }

        }

        private void trackBar_Scroll(object sender, EventArgs e)
        {
            int trackBarId = Int32.Parse(Regex.Match(((TrackBar)sender).Name, @"\d+").Value);
            int trackBarValue = ((TrackBar)sender).Value;
            Control lb = panelTrackBars.Controls["labelTrackBar" + trackBarId];
            lb.Text = lb.Text.Substring(0, lb.Text.LastIndexOf("(") + 1) + "" + trackBarValue + "%)";
            arrProbabilities[trackBarId] = trackBarValue;
        }

        //obliczam straty
        private void calculateLosses(double [,] array)
        {
            arrLosses = new double[array.GetLength(0), array.GetLength(1)];    //wiersze, kolumny
            double maxValueInCol = array[0,0];

            for(int i=0; i<array.GetLength(1); i++) //kolumny
            {
                for(int j=0; j<array.GetLength(0); j++) //wiersze
                {
                    if(maxValueInCol < array[j, i])
                    {
                        maxValueInCol = array[j,i];
                    }
                }

                for (int j = 0; j < array.GetLength(0); j++) //wiersze
                {
                    arrLosses[j, i] = maxValueInCol - array[j, i];
                }
                maxValueInCol = array[0,i];
            }
        }

        private void dataGridViewTablicaWyplat_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvo.clearCellColor((DataGridView)sender);
        }
        

        //KRYTERIA
        private void btnKryteriumHurwicza(object sender, EventArgs e)
        {
            if (!dgvo.isEmptyDgv(dataGridViewTablicaWyplat))
            {
                prepareCalculation();
                double maxValue = criteria.kryteriumHurwicza(arrSalary);
                dgvo.changeCellColor(dataGridViewTablicaWyplat, maxValue);
                infoDominatedDecision(dataGridViewTablicaWyplat);
            }
        }

        private void btnKryteriumWalda(object sender, EventArgs e)
        {
            if (!dgvo.isEmptyDgv(dataGridViewTablicaWyplat))
            {
                prepareCalculation();
                var result = criteria.kryteriumWalda(arrSalary);
                dgvo.changeCellColor(dataGridViewTablicaWyplat, arrSalary, result.Item1, result.Item2);
                infoDominatedDecision(dataGridViewTablicaWyplat);
            }
        }

        private void btnKryteriumSavagea(object sender, EventArgs e)
        {
            if (!dgvo.isEmptyDgv(dataGridViewTablicaWyplat))
            {
                prepareCalculation();
                var result = criteria.kryteriumSavagea(arrLosses);
                dgvo.changeCellColor(dataGridViewTablicaStrat, arrLosses, result.Item1, result.Item2);
                infoDominatedDecision(dataGridViewTablicaWyplat);
            }
        }

        private void btnKryteriumLaplacea(object sender, EventArgs e)
        {
            if (!dgvo.isEmptyDgv(dataGridViewTablicaWyplat))
            {
                prepareCalculation();
                var result = criteria.kryteriumLaplaceaOW(arrSalary, arrProbabilities);
                dgvo.changeHeaderColor(dataGridViewTablicaWyplat, result.Item2);
                dgvo.addTextToRowHeader(dataGridViewTablicaWyplat, dominatedIndex, "OW", result.Item3);
                infoDominatedDecision(dataGridViewTablicaWyplat);
            }
        }

        private void btnKryteriumOW(object sender, EventArgs e)
        {
            if (!dgvo.isEmptyDgv(dataGridViewTablicaWyplat))
            {
                prepareCalculation();
                var result = criteria.kryteriumLaplaceaOW(arrSalary, arrProbabilities, true);
                dgvo.changeHeaderColor(dataGridViewTablicaWyplat, result.Item2);
                dgvo.addTextToRowHeader(dataGridViewTablicaWyplat, dominatedIndex, "OW", result.Item3);
                infoDominatedDecision(dataGridViewTablicaWyplat);
            }
        }

        private void btnKryteriumOSM(object sender, EventArgs e)
        {
            if (!dgvo.isEmptyDgv(dataGridViewTablicaWyplat))
            {
                prepareCalculation();
                var result = criteria.kryteriumOSM(arrLosses, arrProbabilities);
                dgvo.changeHeaderColor(dataGridViewTablicaStrat, result.Item1);
                dgvo.addTextToRowHeader(dataGridViewTablicaStrat, dominatedIndex, "OSM", result.Item2);
                infoDominatedDecision(dataGridViewTablicaWyplat);
            }
        }

        private void btnKryteriumOWDI(object sender, EventArgs e)
        {

        }

        private void btnKryteriumODI(object sender, EventArgs e)
        {
            if (!dgvo.isEmptyDgv(dataGridViewTablicaWyplat))
            {
                prepareCalculation();
                var result = criteria.kryteriumODI(arrSalary, arrProbabilities);

                labelOwdi.Text = "OWDI: " + result.Item1;
                labelOdi.Text = "ODI: " + result.Item2;
                infoDominatedDecision(dataGridViewTablicaWyplat);
            }
        }

        private int extractValue(string text)
        {
            string resultString = Regex.Match(text, @"\d+").Value;
            return Int32.Parse(resultString);
        }

        private void calculateSalary()
        {
            List<int> listDecisions = new List<int>();
            List<int> listStates = new List<int>();
            double[,] array = new double[listViewDecyzje.Items.Count, listViewStany.Items.Count];

            for (int i = 0; i < listViewDecyzje.Items.Count; i++)
            {
                listDecisions.Add(extractValue(listViewDecyzje.Items[i].Text));
            }

            for (int i = 0; i < listViewStany.Items.Count; i++)
            {
                listStates.Add(extractValue(listViewStany.Items[i].Text));
            }

            for (int i = 0; i < listDecisions.Count; i++)
            {
                for (int j = 0; j < listStates.Count; j++)
                {
                    if (listDecisions[i] == listDecisions.Min())
                    {
                        if (listStates[j] >= listDecisions[i])
                        {
                            array[i, j] = (listDecisions[i] * Double.Parse(textBoxCenaRegularna.Text)) - (listDecisions[i] * Double.Parse(textBoxCena1.Text));
                        }
                        else
                        {
                            array[i, j] = (listStates[j] * Double.Parse(textBoxCenaRegularna.Text)) + ((listDecisions[i] - listStates[j]) * Double.Parse(textBoxCenaRabatowa.Text)) - (listDecisions[i] * Double.Parse(textBoxCena1.Text));
                        }
                    }
                    else if (listDecisions[i] == listDecisions.Max())
                    {
                        if (listStates[j] >= listDecisions[i])
                        {
                            array[i, j] = (listDecisions[i] * Double.Parse(textBoxCenaRegularna.Text)) - (listDecisions[i] * Double.Parse(textBoxCena3.Text));
                        }
                        else
                        {
                            array[i, j] = (listStates[j] * Double.Parse(textBoxCenaRegularna.Text)) + ((listDecisions[i] - listStates[j]) * Double.Parse(textBoxCenaRabatowa.Text)) - (listDecisions[i] * Double.Parse(textBoxCena3.Text));
                        }
                    }
                    else
                    {
                        if (listStates[j] >= listDecisions[i])
                        {
                            array[i, j] = (listDecisions[i] * Double.Parse(textBoxCenaRegularna.Text)) - (listDecisions[i] * Double.Parse(textBoxCena2.Text));
                        }
                        else
                        {
                            array[i, j] = (listStates[j] * Double.Parse(textBoxCenaRegularna.Text)) + ((listDecisions[i] - listStates[j]) * Double.Parse(textBoxCenaRabatowa.Text)) - (listDecisions[i] * Double.Parse(textBoxCena2.Text));
                        }
                    }

                }
            }

            ArrayToDgv(array, dataGridViewTablicaWyplat);
        }


        //menadżer przełączania zakładek
        private void tabControlManager()
        {
            cs.visibleControl(btnWstecz, true);
            cs.visibleControl(btnDalej, true);

            switch (tabControl.SelectedIndex)
            {
                case 0:
                    header = "row";
                    cs.enableControl(btnDalej, false);
                    textBox = textBoxDecyzje;
                    listView = listViewDecyzje;
                    btnDodaj = btnDodajDecyzje;
                    btnUsun = btnUsunDecyzje;
                    btnWyczysc = btnWyczyscDecyzje;
                    cs.visibleControl(btnWstecz, false);
                    if(listView.Items.Count >= 2)
                    {
                        cs.enableControl(btnDalej, true);
                    }
                    break;
                case 1:
                    header = "column";
                    cs.enableControl(btnDalej, false);
                    textBox = textBoxStany;
                    listView = listViewStany;
                    btnDodaj = btnDodajStany;
                    btnUsun = btnUsunStany;
                    btnWyczysc = btnWyczyscStany;
                    if (listView.Items.Count >= 2)
                    {
                        cs.enableControl(btnDalej, true);
                    }
                    break;
                case 3:
                    createControls(listViewStany);
                    break;
                case 4:
                    cs.visibleControl(btnDalej, false);
                    dgvo.clearCellColor(dataGridViewTablicaWyplat);
                    dgvo.clearCellColor(dataGridViewTablicaStrat);
                    dgvo.resetRowHeader(dataGridViewTablicaWyplat);
                    dgvo.resetRowHeader(dataGridViewTablicaStrat);
                    labelDecyzjeZdominowane.Text = "DECYZJE ZDOMINOWANE: ";
                    labelOwdi.Text = "OWDI: ";
                    labelOdi.Text = "ODI: ";
                    dgvo.addColumnsToDataGridViewFromListView(dataGridViewTablicaWyplat, listViewStany);
                    dgvo.addRowsToDataGridViewFromListView(dataGridViewTablicaWyplat, listViewDecyzje);

                    dgvo.addColumnsToDataGridViewFromListView(dataGridViewTablicaStrat, listViewStany);
                    dgvo.addRowsToDataGridViewFromListView(dataGridViewTablicaStrat, listViewDecyzje);

                    if(textBoxCena1.Text != null && textBoxCena1.Text != "" &&
                        textBoxCena2.Text != null && textBoxCena2.Text != "" &&
                        textBoxCena3.Text != null && textBoxCena3.Text != "" &&
                        textBoxCenaRegularna.Text != null && textBoxCenaRegularna.Text != "" &&
                        textBoxCenaRabatowa.Text != null && textBoxCenaRabatowa.Text != "")
                    {
                        calculateSalary();
                    }

                    break;
            }
            //controlsSetup();
        }


        private void controlsSetup()
        {
            cs.sizeControl(textBoxDecyzje, "mid");
            cs.centerControl(textBoxDecyzje);

            cs.sizeControl(btnDodajDecyzje, "mid");
            cs.centerControl(btnDodajDecyzje);

            listViewDecyzje.Width = 400;
            listViewDecyzje.Columns[0].Width = 400 - SystemInformation.VerticalScrollBarWidth;
            cs.centerControl(listViewDecyzje);
            listViewDecyzje.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom);

            cs.sizeControl(btnWyczyscDecyzje, "sml");
            cs.leftControl(btnWyczyscDecyzje);

            cs.sizeControl(btnUsunDecyzje, "sml");
            cs.rightControl(btnUsunDecyzje);


            cs.sizeControl(textBoxStany, "mid");
            cs.centerControl(textBoxStany);

            cs.sizeControl(btnDodajStany, "mid");
            cs.centerControl(btnDodajStany);

            listViewStany.Width = 400;
            listViewStany.Columns[0].Width = 400 - SystemInformation.VerticalScrollBarWidth;
            cs.centerControl(listViewStany);
            listViewStany.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom);

            cs.sizeControl(btnWyczyscStany, "sml");
            cs.leftControl(btnWyczyscStany);

            cs.sizeControl(btnUsunStany, "sml");
            cs.rightControl(btnUsunStany);

            cs.sizeControl(btnDalej, "sml");

            cs.sizeControl(btnWstecz, "sml");

            panelTrackBars.Width = 450;
            cs.centerControl(panelTrackBars);

            cs.sizeControl(btnKryterium1, "sml");
            cs.rightControlMax(btnKryterium1);

            cs.sizeControl(btnKryterium2, "sml");
            cs.rightControlMax(btnKryterium2);

            cs.sizeControl(btnKryterium3, "sml");
            cs.rightControlMax(btnKryterium3);

            cs.sizeControl(btnKryterium4, "sml");
            cs.rightControlMax(btnKryterium4);

            cs.sizeControl(btnKryterium5, "sml");
            cs.rightControlMax(btnKryterium5);

            cs.sizeControl(btnKryterium6, "sml");
            cs.rightControlMax(btnKryterium6);

            cs.sizeControl(btnKryterium7, "sml");
            cs.rightControlMax(btnKryterium7);

            cs.sizeControl(labelCena1, "mid");
            cs.sizeControl(labelCena2, "mid");
            cs.sizeControl(labelCena3, "mid");
            cs.sizeControl(labelCenaRegularna, "mid");
            cs.sizeControl(labelCenaRabatowa, "mid");

            cs.centerControl(labelCena1);
            cs.centerControl(labelCena2);
            cs.centerControl(labelCena3);
            cs.centerControl(labelCenaRegularna);
            cs.centerControl(labelCenaRabatowa);

            cs.sizeControl(textBoxCena1, "mid");
            cs.sizeControl(textBoxCena2, "mid");
            cs.sizeControl(textBoxCena3, "mid");
            cs.sizeControl(textBoxCenaRegularna, "mid");
            cs.sizeControl(textBoxCenaRabatowa, "mid");

            cs.centerControl(textBoxCena1);
            cs.centerControl(textBoxCena2);
            cs.centerControl(textBoxCena3);
            cs.centerControl(textBoxCenaRegularna);
            cs.centerControl(textBoxCenaRabatowa);
        }

        private void btnKryterium_MouseEnter(object sender, EventArgs e)
        {
            Thread.Sleep(150);
            labelHint.Text = hintText(sender);
            labelHint.Show();
        }

        private void btnKryterium_MouseLeave(object sender, EventArgs e)
        {
            labelHint.Hide();
        }

        private void btnKryterium_MouseMove(object sender, MouseEventArgs e)
        {
            var mouseY = e.Location.Y;
            var mouseX = e.Location.X;
            var btnX = ((MaterialRaisedButton)sender).Location.X;
            var btnY = ((MaterialRaisedButton)sender).Location.Y;

            labelHint.Location = new Point(mouseX + btnX - labelHint.Width - 5, mouseY + btnY + 5);
        }

        private string hintText(object sender)
        {
            string btnKryteriumName = ((MaterialRaisedButton)sender).Name;
            string textLabel = "";
            switch (btnKryteriumName)
            {
                case "btnKryterium1":
                    textLabel = "Wskazuje najwyższą wypłatę spośród wszystkich wypłat.";
                    break;
                case "btnKryterium2":
                    textLabel = "Wskazuje najwyższą wypłatę spośród najniższych wypłat każdej dezycji.";            
                    break;
                case "btnKryterium3":
                    textLabel = "Wskazuje najmniejszą stratę spośród najwyższych strat każdej dezycji.";
                    break;
                case "btnKryterium4":
                    textLabel = "Wskazuje najlepszą dezycję na podstawie zysków zakładając takie samo prawdopodobieństwo wystąpienia każdego stanu natury.";
                    break;
                case "btnKryterium5":
                    textLabel = "Wskazuje najlepszą dezycję na podstawie zysków  biorąc pod uwagę ustawione prawdopodobieństwo wystąpienia stanu natury.";
                    break;
                case "btnKryterium6":
                    textLabel = "Wskazuje najlepszą dezycję na podstawie kosztów biorąc pod uwagę ustawione prawdopodobieństwo wystąpienia stanu natury.";
                    break;
                case "btnKryterium7":
                    textLabel = "Oblicza oczekiwaną wypłatę przy wykorzystaniu doskonałej informacji (OWDI) oraz koszt jej uzyskania (ODI).";
                    break;
            }
            return textLabel;
        }
    }
}
