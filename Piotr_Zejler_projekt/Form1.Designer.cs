﻿namespace Piotr_Zejler_projekt
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new MaterialSkin.Controls.MaterialTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnUsunDecyzje = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnWyczyscDecyzje = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnDodajDecyzje = new MaterialSkin.Controls.MaterialRaisedButton();
            this.listViewDecyzje = new MaterialSkin.Controls.MaterialListView();
            this.columnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxDecyzje = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnUsunStany = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnWyczyscStany = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnDodajStany = new MaterialSkin.Controls.MaterialRaisedButton();
            this.listViewStany = new MaterialSkin.Controls.MaterialListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxStany = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBoxCenaRabatowa = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.textBoxCenaRegularna = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.textBoxCena3 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.textBoxCena2 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.textBoxCena1 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.labelCenaRabatowa = new System.Windows.Forms.Label();
            this.labelCenaRegularna = new System.Windows.Forms.Label();
            this.labelCena3 = new System.Windows.Forms.Label();
            this.labelCena2 = new System.Windows.Forms.Label();
            this.labelCena1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panelTrackBars = new System.Windows.Forms.Panel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.labelHint = new System.Windows.Forms.Label();
            this.btnKryterium7 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.labelOdi = new System.Windows.Forms.Label();
            this.labelOwdi = new System.Windows.Forms.Label();
            this.labelDecyzjeZdominowane = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelTablicaStrat = new System.Windows.Forms.Label();
            this.labelTablicaWypłat = new System.Windows.Forms.Label();
            this.dataGridViewTablicaStrat = new System.Windows.Forms.DataGridView();
            this.dataGridViewTablicaWyplat = new System.Windows.Forms.DataGridView();
            this.btnKryterium6 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnKryterium5 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnKryterium4 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnKryterium3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnKryterium2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnKryterium1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialTabSelector1 = new MaterialSkin.Controls.MaterialTabSelector();
            this.btnDalej = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnWstecz = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTablicaStrat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTablicaWyplat)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Depth = 0;
            this.tabControl.Location = new System.Drawing.Point(15, 197);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1771, 806);
            this.tabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.btnUsunDecyzje);
            this.tabPage1.Controls.Add(this.btnWyczyscDecyzje);
            this.tabPage1.Controls.Add(this.btnDodajDecyzje);
            this.tabPage1.Controls.Add(this.listViewDecyzje);
            this.tabPage1.Controls.Add(this.textBoxDecyzje);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1763, 769);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DECYZJE";
            // 
            // btnUsunDecyzje
            // 
            this.btnUsunDecyzje.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnUsunDecyzje.AutoSize = true;
            this.btnUsunDecyzje.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUsunDecyzje.Depth = 0;
            this.btnUsunDecyzje.Icon = null;
            this.btnUsunDecyzje.Location = new System.Drawing.Point(1047, 577);
            this.btnUsunDecyzje.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.btnUsunDecyzje.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnUsunDecyzje.Name = "btnUsunDecyzje";
            this.btnUsunDecyzje.Primary = false;
            this.btnUsunDecyzje.Size = new System.Drawing.Size(87, 36);
            this.btnUsunDecyzje.TabIndex = 6;
            this.btnUsunDecyzje.Text = "USUŃ";
            this.btnUsunDecyzje.UseVisualStyleBackColor = true;
            this.btnUsunDecyzje.Click += new System.EventHandler(this.btnUsun_Click);
            // 
            // btnWyczyscDecyzje
            // 
            this.btnWyczyscDecyzje.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnWyczyscDecyzje.AutoSize = true;
            this.btnWyczyscDecyzje.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnWyczyscDecyzje.BackColor = System.Drawing.Color.DimGray;
            this.btnWyczyscDecyzje.Depth = 0;
            this.btnWyczyscDecyzje.Icon = null;
            this.btnWyczyscDecyzje.Location = new System.Drawing.Point(518, 577);
            this.btnWyczyscDecyzje.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.btnWyczyscDecyzje.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnWyczyscDecyzje.Name = "btnWyczyscDecyzje";
            this.btnWyczyscDecyzje.Primary = false;
            this.btnWyczyscDecyzje.Size = new System.Drawing.Size(135, 36);
            this.btnWyczyscDecyzje.TabIndex = 5;
            this.btnWyczyscDecyzje.Text = "WYCZYŚĆ";
            this.btnWyczyscDecyzje.UseVisualStyleBackColor = false;
            this.btnWyczyscDecyzje.Click += new System.EventHandler(this.btnWyczysc_Click);
            // 
            // btnDodajDecyzje
            // 
            this.btnDodajDecyzje.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDodajDecyzje.AutoSize = true;
            this.btnDodajDecyzje.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDodajDecyzje.Depth = 0;
            this.btnDodajDecyzje.Icon = null;
            this.btnDodajDecyzje.Location = new System.Drawing.Point(518, 94);
            this.btnDodajDecyzje.Margin = new System.Windows.Forms.Padding(4);
            this.btnDodajDecyzje.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnDodajDecyzje.Name = "btnDodajDecyzje";
            this.btnDodajDecyzje.Primary = true;
            this.btnDodajDecyzje.Size = new System.Drawing.Size(102, 36);
            this.btnDodajDecyzje.TabIndex = 2;
            this.btnDodajDecyzje.Text = "DODAJ";
            this.btnDodajDecyzje.UseVisualStyleBackColor = true;
            this.btnDodajDecyzje.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // listViewDecyzje
            // 
            this.listViewDecyzje.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.listViewDecyzje.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.listViewDecyzje.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewDecyzje.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader});
            this.listViewDecyzje.Depth = 0;
            this.listViewDecyzje.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.listViewDecyzje.FullRowSelect = true;
            this.listViewDecyzje.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewDecyzje.Location = new System.Drawing.Point(518, 174);
            this.listViewDecyzje.Margin = new System.Windows.Forms.Padding(4);
            this.listViewDecyzje.MouseLocation = new System.Drawing.Point(-1, -1);
            this.listViewDecyzje.MouseState = MaterialSkin.MouseState.OUT;
            this.listViewDecyzje.Name = "listViewDecyzje";
            this.listViewDecyzje.OwnerDraw = true;
            this.listViewDecyzje.Size = new System.Drawing.Size(634, 359);
            this.listViewDecyzje.TabIndex = 1;
            this.listViewDecyzje.UseCompatibleStateImageBehavior = false;
            this.listViewDecyzje.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader
            // 
            this.columnHeader.Width = 400;
            // 
            // textBoxDecyzje
            // 
            this.textBoxDecyzje.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxDecyzje.Depth = 0;
            this.textBoxDecyzje.Hint = "                                      Wpisz decyzję";
            this.textBoxDecyzje.Location = new System.Drawing.Point(518, 38);
            this.textBoxDecyzje.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDecyzje.MaxLength = 32767;
            this.textBoxDecyzje.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBoxDecyzje.Name = "textBoxDecyzje";
            this.textBoxDecyzje.PasswordChar = '\0';
            this.textBoxDecyzje.SelectedText = "";
            this.textBoxDecyzje.SelectionLength = 0;
            this.textBoxDecyzje.SelectionStart = 0;
            this.textBoxDecyzje.Size = new System.Drawing.Size(634, 36);
            this.textBoxDecyzje.TabIndex = 0;
            this.textBoxDecyzje.TabStop = false;
            this.textBoxDecyzje.UseSystemPasswordChar = false;
            this.textBoxDecyzje.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.btnUsunStany);
            this.tabPage2.Controls.Add(this.btnWyczyscStany);
            this.tabPage2.Controls.Add(this.btnDodajStany);
            this.tabPage2.Controls.Add(this.listViewStany);
            this.tabPage2.Controls.Add(this.textBoxStany);
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1763, 769);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "STANY NATURY";
            // 
            // btnUsunStany
            // 
            this.btnUsunStany.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnUsunStany.AutoSize = true;
            this.btnUsunStany.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnUsunStany.Depth = 0;
            this.btnUsunStany.Icon = null;
            this.btnUsunStany.Location = new System.Drawing.Point(947, 577);
            this.btnUsunStany.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.btnUsunStany.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnUsunStany.Name = "btnUsunStany";
            this.btnUsunStany.Primary = false;
            this.btnUsunStany.Size = new System.Drawing.Size(87, 36);
            this.btnUsunStany.TabIndex = 11;
            this.btnUsunStany.Text = "USUŃ";
            this.btnUsunStany.UseVisualStyleBackColor = true;
            this.btnUsunStany.Click += new System.EventHandler(this.btnUsun_Click);
            // 
            // btnWyczyscStany
            // 
            this.btnWyczyscStany.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnWyczyscStany.AutoSize = true;
            this.btnWyczyscStany.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnWyczyscStany.BackColor = System.Drawing.Color.DimGray;
            this.btnWyczyscStany.Depth = 0;
            this.btnWyczyscStany.Icon = null;
            this.btnWyczyscStany.Location = new System.Drawing.Point(457, 577);
            this.btnWyczyscStany.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.btnWyczyscStany.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnWyczyscStany.Name = "btnWyczyscStany";
            this.btnWyczyscStany.Primary = false;
            this.btnWyczyscStany.Size = new System.Drawing.Size(135, 36);
            this.btnWyczyscStany.TabIndex = 10;
            this.btnWyczyscStany.Text = "WYCZYŚĆ";
            this.btnWyczyscStany.UseVisualStyleBackColor = false;
            this.btnWyczyscStany.Click += new System.EventHandler(this.btnWyczysc_Click);
            // 
            // btnDodajStany
            // 
            this.btnDodajStany.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDodajStany.AutoSize = true;
            this.btnDodajStany.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDodajStany.Depth = 0;
            this.btnDodajStany.Icon = null;
            this.btnDodajStany.Location = new System.Drawing.Point(446, 94);
            this.btnDodajStany.Margin = new System.Windows.Forms.Padding(4);
            this.btnDodajStany.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnDodajStany.Name = "btnDodajStany";
            this.btnDodajStany.Primary = true;
            this.btnDodajStany.Size = new System.Drawing.Size(102, 36);
            this.btnDodajStany.TabIndex = 7;
            this.btnDodajStany.Text = "DODAJ";
            this.btnDodajStany.UseVisualStyleBackColor = true;
            this.btnDodajStany.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // listViewStany
            // 
            this.listViewStany.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.listViewStany.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.listViewStany.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewStany.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listViewStany.Depth = 0;
            this.listViewStany.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.listViewStany.FullRowSelect = true;
            this.listViewStany.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewStany.Location = new System.Drawing.Point(446, 174);
            this.listViewStany.Margin = new System.Windows.Forms.Padding(4);
            this.listViewStany.MouseLocation = new System.Drawing.Point(-1, -1);
            this.listViewStany.MouseState = MaterialSkin.MouseState.OUT;
            this.listViewStany.Name = "listViewStany";
            this.listViewStany.OwnerDraw = true;
            this.listViewStany.Size = new System.Drawing.Size(634, 372);
            this.listViewStany.TabIndex = 6;
            this.listViewStany.UseCompatibleStateImageBehavior = false;
            this.listViewStany.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 400;
            // 
            // textBoxStany
            // 
            this.textBoxStany.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxStany.Depth = 0;
            this.textBoxStany.Hint = "                                   Wpisz stan natury";
            this.textBoxStany.Location = new System.Drawing.Point(446, 38);
            this.textBoxStany.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxStany.MaxLength = 32767;
            this.textBoxStany.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBoxStany.Name = "textBoxStany";
            this.textBoxStany.PasswordChar = '\0';
            this.textBoxStany.SelectedText = "";
            this.textBoxStany.SelectionLength = 0;
            this.textBoxStany.SelectionStart = 0;
            this.textBoxStany.Size = new System.Drawing.Size(634, 36);
            this.textBoxStany.TabIndex = 5;
            this.textBoxStany.TabStop = false;
            this.textBoxStany.UseSystemPasswordChar = false;
            this.textBoxStany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_KeyDown);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.White;
            this.tabPage5.Controls.Add(this.textBoxCenaRabatowa);
            this.tabPage5.Controls.Add(this.textBoxCenaRegularna);
            this.tabPage5.Controls.Add(this.textBoxCena3);
            this.tabPage5.Controls.Add(this.textBoxCena2);
            this.tabPage5.Controls.Add(this.textBoxCena1);
            this.tabPage5.Controls.Add(this.labelCenaRabatowa);
            this.tabPage5.Controls.Add(this.labelCenaRegularna);
            this.tabPage5.Controls.Add(this.labelCena3);
            this.tabPage5.Controls.Add(this.labelCena2);
            this.tabPage5.Controls.Add(this.labelCena1);
            this.tabPage5.Location = new System.Drawing.Point(4, 33);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1763, 769);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "CENY";
            // 
            // textBoxCenaRabatowa
            // 
            this.textBoxCenaRabatowa.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxCenaRabatowa.Depth = 0;
            this.textBoxCenaRabatowa.Hint = "";
            this.textBoxCenaRabatowa.Location = new System.Drawing.Point(490, 513);
            this.textBoxCenaRabatowa.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCenaRabatowa.MaxLength = 32767;
            this.textBoxCenaRabatowa.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBoxCenaRabatowa.Name = "textBoxCenaRabatowa";
            this.textBoxCenaRabatowa.PasswordChar = '\0';
            this.textBoxCenaRabatowa.SelectedText = "";
            this.textBoxCenaRabatowa.SelectionLength = 0;
            this.textBoxCenaRabatowa.SelectionStart = 0;
            this.textBoxCenaRabatowa.Size = new System.Drawing.Size(634, 36);
            this.textBoxCenaRabatowa.TabIndex = 15;
            this.textBoxCenaRabatowa.TabStop = false;
            this.textBoxCenaRabatowa.UseSystemPasswordChar = false;
            // 
            // textBoxCenaRegularna
            // 
            this.textBoxCenaRegularna.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxCenaRegularna.Depth = 0;
            this.textBoxCenaRegularna.Hint = "";
            this.textBoxCenaRegularna.Location = new System.Drawing.Point(490, 412);
            this.textBoxCenaRegularna.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCenaRegularna.MaxLength = 32767;
            this.textBoxCenaRegularna.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBoxCenaRegularna.Name = "textBoxCenaRegularna";
            this.textBoxCenaRegularna.PasswordChar = '\0';
            this.textBoxCenaRegularna.SelectedText = "";
            this.textBoxCenaRegularna.SelectionLength = 0;
            this.textBoxCenaRegularna.SelectionStart = 0;
            this.textBoxCenaRegularna.Size = new System.Drawing.Size(634, 36);
            this.textBoxCenaRegularna.TabIndex = 14;
            this.textBoxCenaRegularna.TabStop = false;
            this.textBoxCenaRegularna.UseSystemPasswordChar = false;
            // 
            // textBoxCena3
            // 
            this.textBoxCena3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxCena3.Depth = 0;
            this.textBoxCena3.Hint = "";
            this.textBoxCena3.Location = new System.Drawing.Point(490, 311);
            this.textBoxCena3.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCena3.MaxLength = 32767;
            this.textBoxCena3.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBoxCena3.Name = "textBoxCena3";
            this.textBoxCena3.PasswordChar = '\0';
            this.textBoxCena3.SelectedText = "";
            this.textBoxCena3.SelectionLength = 0;
            this.textBoxCena3.SelectionStart = 0;
            this.textBoxCena3.Size = new System.Drawing.Size(634, 36);
            this.textBoxCena3.TabIndex = 13;
            this.textBoxCena3.TabStop = false;
            this.textBoxCena3.UseSystemPasswordChar = false;
            // 
            // textBoxCena2
            // 
            this.textBoxCena2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxCena2.Depth = 0;
            this.textBoxCena2.Hint = "";
            this.textBoxCena2.Location = new System.Drawing.Point(490, 210);
            this.textBoxCena2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCena2.MaxLength = 32767;
            this.textBoxCena2.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBoxCena2.Name = "textBoxCena2";
            this.textBoxCena2.PasswordChar = '\0';
            this.textBoxCena2.SelectedText = "";
            this.textBoxCena2.SelectionLength = 0;
            this.textBoxCena2.SelectionStart = 0;
            this.textBoxCena2.Size = new System.Drawing.Size(634, 36);
            this.textBoxCena2.TabIndex = 12;
            this.textBoxCena2.TabStop = false;
            this.textBoxCena2.UseSystemPasswordChar = false;
            // 
            // textBoxCena1
            // 
            this.textBoxCena1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxCena1.Depth = 0;
            this.textBoxCena1.Hint = "";
            this.textBoxCena1.Location = new System.Drawing.Point(490, 109);
            this.textBoxCena1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCena1.MaxLength = 32767;
            this.textBoxCena1.MouseState = MaterialSkin.MouseState.HOVER;
            this.textBoxCena1.Name = "textBoxCena1";
            this.textBoxCena1.PasswordChar = '\0';
            this.textBoxCena1.SelectedText = "";
            this.textBoxCena1.SelectionLength = 0;
            this.textBoxCena1.SelectionStart = 0;
            this.textBoxCena1.Size = new System.Drawing.Size(634, 36);
            this.textBoxCena1.TabIndex = 11;
            this.textBoxCena1.TabStop = false;
            this.textBoxCena1.UseSystemPasswordChar = false;
            // 
            // labelCenaRabatowa
            // 
            this.labelCenaRabatowa.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelCenaRabatowa.AutoSize = true;
            this.labelCenaRabatowa.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCenaRabatowa.ForeColor = System.Drawing.Color.DarkGray;
            this.labelCenaRabatowa.Location = new System.Drawing.Point(484, 466);
            this.labelCenaRabatowa.Name = "labelCenaRabatowa";
            this.labelCenaRabatowa.Size = new System.Drawing.Size(257, 32);
            this.labelCenaRabatowa.TabIndex = 10;
            this.labelCenaRabatowa.Text = "CENA RABATOWA";
            // 
            // labelCenaRegularna
            // 
            this.labelCenaRegularna.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelCenaRegularna.AutoSize = true;
            this.labelCenaRegularna.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCenaRegularna.ForeColor = System.Drawing.Color.DarkGray;
            this.labelCenaRegularna.Location = new System.Drawing.Point(484, 365);
            this.labelCenaRegularna.Name = "labelCenaRegularna";
            this.labelCenaRegularna.Size = new System.Drawing.Size(274, 32);
            this.labelCenaRegularna.TabIndex = 8;
            this.labelCenaRegularna.Text = "CENA REGULARNA";
            // 
            // labelCena3
            // 
            this.labelCena3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelCena3.AutoSize = true;
            this.labelCena3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCena3.ForeColor = System.Drawing.Color.DarkGray;
            this.labelCena3.Location = new System.Drawing.Point(484, 264);
            this.labelCena3.Name = "labelCena3";
            this.labelCena3.Size = new System.Drawing.Size(263, 32);
            this.labelCena3.TabIndex = 6;
            this.labelCena3.Text = "CENA ZA 3 PARTIE";
            // 
            // labelCena2
            // 
            this.labelCena2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelCena2.AutoSize = true;
            this.labelCena2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCena2.ForeColor = System.Drawing.Color.DarkGray;
            this.labelCena2.Location = new System.Drawing.Point(484, 163);
            this.labelCena2.Name = "labelCena2";
            this.labelCena2.Size = new System.Drawing.Size(263, 32);
            this.labelCena2.TabIndex = 4;
            this.labelCena2.Text = "CENA ZA 2 PARTIE";
            // 
            // labelCena1
            // 
            this.labelCena1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelCena1.AutoSize = true;
            this.labelCena1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCena1.ForeColor = System.Drawing.Color.DarkGray;
            this.labelCena1.Location = new System.Drawing.Point(484, 62);
            this.labelCena1.Name = "labelCena1";
            this.labelCena1.Size = new System.Drawing.Size(263, 32);
            this.labelCena1.TabIndex = 2;
            this.labelCena1.Text = "CENA ZA 1 PARTIĘ";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panelTrackBars);
            this.tabPage3.Location = new System.Drawing.Point(4, 33);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage3.Size = new System.Drawing.Size(1763, 769);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "PRAWDOPODOBIEŃSTWA";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panelTrackBars
            // 
            this.panelTrackBars.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.panelTrackBars.AutoScroll = true;
            this.panelTrackBars.BackColor = System.Drawing.Color.Transparent;
            this.panelTrackBars.Location = new System.Drawing.Point(600, 20);
            this.panelTrackBars.Margin = new System.Windows.Forms.Padding(4);
            this.panelTrackBars.Name = "panelTrackBars";
            this.panelTrackBars.Size = new System.Drawing.Size(244, 600);
            this.panelTrackBars.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.labelHint);
            this.tabPage4.Controls.Add(this.btnKryterium7);
            this.tabPage4.Controls.Add(this.labelOdi);
            this.tabPage4.Controls.Add(this.labelOwdi);
            this.tabPage4.Controls.Add(this.labelDecyzjeZdominowane);
            this.tabPage4.Controls.Add(this.panel1);
            this.tabPage4.Controls.Add(this.btnKryterium6);
            this.tabPage4.Controls.Add(this.btnKryterium5);
            this.tabPage4.Controls.Add(this.btnKryterium4);
            this.tabPage4.Controls.Add(this.btnKryterium3);
            this.tabPage4.Controls.Add(this.btnKryterium2);
            this.tabPage4.Controls.Add(this.btnKryterium1);
            this.tabPage4.Location = new System.Drawing.Point(4, 33);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage4.Size = new System.Drawing.Size(1763, 769);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "OBLICZENIA";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // labelHint
            // 
            this.labelHint.AutoSize = true;
            this.labelHint.BackColor = System.Drawing.Color.Cornsilk;
            this.labelHint.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHint.Location = new System.Drawing.Point(1212, 415);
            this.labelHint.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHint.MaximumSize = new System.Drawing.Size(367, 0);
            this.labelHint.Name = "labelHint";
            this.labelHint.Padding = new System.Windows.Forms.Padding(10);
            this.labelHint.Size = new System.Drawing.Size(356, 107);
            this.labelHint.TabIndex = 17;
            this.labelHint.Text = "Tutaj wpiszę sobie tekst, w którym opiszę jak działa dane kryterium";
            // 
            // btnKryterium7
            // 
            this.btnKryterium7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKryterium7.AutoSize = true;
            this.btnKryterium7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnKryterium7.Depth = 0;
            this.btnKryterium7.Icon = null;
            this.btnKryterium7.Location = new System.Drawing.Point(1279, 526);
            this.btnKryterium7.Margin = new System.Windows.Forms.Padding(4);
            this.btnKryterium7.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnKryterium7.Name = "btnKryterium7";
            this.btnKryterium7.Primary = true;
            this.btnKryterium7.Size = new System.Drawing.Size(222, 36);
            this.btnKryterium7.TabIndex = 16;
            this.btnKryterium7.Text = "KRYTERIUM OWDI";
            this.btnKryterium7.UseVisualStyleBackColor = true;
            this.btnKryterium7.Click += new System.EventHandler(this.btnKryteriumODI);
            this.btnKryterium7.MouseEnter += new System.EventHandler(this.btnKryterium_MouseEnter);
            this.btnKryterium7.MouseLeave += new System.EventHandler(this.btnKryterium_MouseLeave);
            this.btnKryterium7.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnKryterium_MouseMove);
            // 
            // labelOdi
            // 
            this.labelOdi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOdi.AutoSize = true;
            this.labelOdi.Font = new System.Drawing.Font("Arial", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOdi.ForeColor = System.Drawing.Color.DarkGray;
            this.labelOdi.Location = new System.Drawing.Point(1404, 694);
            this.labelOdi.Name = "labelOdi";
            this.labelOdi.Size = new System.Drawing.Size(97, 40);
            this.labelOdi.TabIndex = 15;
            this.labelOdi.Text = "ODI: ";
            // 
            // labelOwdi
            // 
            this.labelOwdi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOwdi.AutoSize = true;
            this.labelOwdi.Font = new System.Drawing.Font("Arial", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOwdi.ForeColor = System.Drawing.Color.DarkGray;
            this.labelOwdi.Location = new System.Drawing.Point(1398, 630);
            this.labelOwdi.Name = "labelOwdi";
            this.labelOwdi.Size = new System.Drawing.Size(128, 40);
            this.labelOwdi.TabIndex = 14;
            this.labelOwdi.Text = "OWDI: ";
            // 
            // labelDecyzjeZdominowane
            // 
            this.labelDecyzjeZdominowane.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDecyzjeZdominowane.AutoSize = true;
            this.labelDecyzjeZdominowane.Font = new System.Drawing.Font("Arial", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDecyzjeZdominowane.ForeColor = System.Drawing.Color.DarkGray;
            this.labelDecyzjeZdominowane.Location = new System.Drawing.Point(11, 660);
            this.labelDecyzjeZdominowane.Name = "labelDecyzjeZdominowane";
            this.labelDecyzjeZdominowane.Size = new System.Drawing.Size(457, 40);
            this.labelDecyzjeZdominowane.TabIndex = 13;
            this.labelDecyzjeZdominowane.Text = "DECYZJE ZDOMINOWANE:";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.labelTablicaStrat);
            this.panel1.Controls.Add(this.labelTablicaWypłat);
            this.panel1.Controls.Add(this.dataGridViewTablicaStrat);
            this.panel1.Controls.Add(this.dataGridViewTablicaWyplat);
            this.panel1.Location = new System.Drawing.Point(1, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1375, 651);
            this.panel1.TabIndex = 11;
            // 
            // labelTablicaStrat
            // 
            this.labelTablicaStrat.AutoSize = true;
            this.labelTablicaStrat.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTablicaStrat.ForeColor = System.Drawing.Color.DarkGray;
            this.labelTablicaStrat.Location = new System.Drawing.Point(6, 585);
            this.labelTablicaStrat.Name = "labelTablicaStrat";
            this.labelTablicaStrat.Size = new System.Drawing.Size(479, 65);
            this.labelTablicaStrat.TabIndex = 3;
            this.labelTablicaStrat.Text = "TABILICA STRAT";
            // 
            // labelTablicaWypłat
            // 
            this.labelTablicaWypłat.AutoSize = true;
            this.labelTablicaWypłat.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTablicaWypłat.ForeColor = System.Drawing.Color.DarkGray;
            this.labelTablicaWypłat.Location = new System.Drawing.Point(6, 7);
            this.labelTablicaWypłat.Name = "labelTablicaWypłat";
            this.labelTablicaWypłat.Size = new System.Drawing.Size(529, 65);
            this.labelTablicaWypłat.TabIndex = 2;
            this.labelTablicaWypłat.Text = "TABILICA WYPŁAT";
            // 
            // dataGridViewTablicaStrat
            // 
            this.dataGridViewTablicaStrat.AllowUserToAddRows = false;
            this.dataGridViewTablicaStrat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewTablicaStrat.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.dataGridViewTablicaStrat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Turquoise;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTablicaStrat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTablicaStrat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTablicaStrat.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTablicaStrat.Enabled = false;
            this.dataGridViewTablicaStrat.EnableHeadersVisualStyles = false;
            this.dataGridViewTablicaStrat.Location = new System.Drawing.Point(39, 669);
            this.dataGridViewTablicaStrat.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewTablicaStrat.MinimumSize = new System.Drawing.Size(1302, 469);
            this.dataGridViewTablicaStrat.MultiSelect = false;
            this.dataGridViewTablicaStrat.Name = "dataGridViewTablicaStrat";
            this.dataGridViewTablicaStrat.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Turquoise;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTablicaStrat.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTablicaStrat.RowHeadersWidth = 60;
            this.dataGridViewTablicaStrat.RowTemplate.Height = 40;
            this.dataGridViewTablicaStrat.Size = new System.Drawing.Size(1302, 469);
            this.dataGridViewTablicaStrat.TabIndex = 1;
            this.dataGridViewTablicaStrat.Paint += new System.Windows.Forms.PaintEventHandler(this.dataGridView_Paint);
            // 
            // dataGridViewTablicaWyplat
            // 
            this.dataGridViewTablicaWyplat.AllowUserToAddRows = false;
            this.dataGridViewTablicaWyplat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewTablicaWyplat.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.dataGridViewTablicaWyplat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Turquoise;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTablicaWyplat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTablicaWyplat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Turquoise;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTablicaWyplat.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTablicaWyplat.EnableHeadersVisualStyles = false;
            this.dataGridViewTablicaWyplat.Location = new System.Drawing.Point(39, 90);
            this.dataGridViewTablicaWyplat.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewTablicaWyplat.MinimumSize = new System.Drawing.Size(1302, 469);
            this.dataGridViewTablicaWyplat.MultiSelect = false;
            this.dataGridViewTablicaWyplat.Name = "dataGridViewTablicaWyplat";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Turquoise;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTablicaWyplat.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTablicaWyplat.RowHeadersWidth = 60;
            this.dataGridViewTablicaWyplat.RowTemplate.Height = 40;
            this.dataGridViewTablicaWyplat.Size = new System.Drawing.Size(1302, 469);
            this.dataGridViewTablicaWyplat.TabIndex = 0;
            this.dataGridViewTablicaWyplat.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewTablicaWyplat_CellClick);
            this.dataGridViewTablicaWyplat.Paint += new System.Windows.Forms.PaintEventHandler(this.dataGridView_Paint);
            // 
            // btnKryterium6
            // 
            this.btnKryterium6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKryterium6.AutoSize = true;
            this.btnKryterium6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnKryterium6.Depth = 0;
            this.btnKryterium6.Icon = null;
            this.btnKryterium6.Location = new System.Drawing.Point(1298, 440);
            this.btnKryterium6.Margin = new System.Windows.Forms.Padding(4);
            this.btnKryterium6.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnKryterium6.Name = "btnKryterium6";
            this.btnKryterium6.Primary = true;
            this.btnKryterium6.Size = new System.Drawing.Size(214, 36);
            this.btnKryterium6.TabIndex = 9;
            this.btnKryterium6.Text = "KRYTERIUM OSM";
            this.btnKryterium6.UseVisualStyleBackColor = true;
            this.btnKryterium6.Click += new System.EventHandler(this.btnKryteriumOSM);
            this.btnKryterium6.MouseEnter += new System.EventHandler(this.btnKryterium_MouseEnter);
            this.btnKryterium6.MouseLeave += new System.EventHandler(this.btnKryterium_MouseLeave);
            this.btnKryterium6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnKryterium_MouseMove);
            // 
            // btnKryterium5
            // 
            this.btnKryterium5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKryterium5.AutoSize = true;
            this.btnKryterium5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnKryterium5.Depth = 0;
            this.btnKryterium5.Icon = null;
            this.btnKryterium5.Location = new System.Drawing.Point(1294, 354);
            this.btnKryterium5.Margin = new System.Windows.Forms.Padding(4);
            this.btnKryterium5.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnKryterium5.Name = "btnKryterium5";
            this.btnKryterium5.Primary = true;
            this.btnKryterium5.Size = new System.Drawing.Size(199, 36);
            this.btnKryterium5.TabIndex = 8;
            this.btnKryterium5.Text = "KRYTERIUM OW";
            this.btnKryterium5.UseVisualStyleBackColor = true;
            this.btnKryterium5.Click += new System.EventHandler(this.btnKryteriumOW);
            this.btnKryterium5.MouseEnter += new System.EventHandler(this.btnKryterium_MouseEnter);
            this.btnKryterium5.MouseLeave += new System.EventHandler(this.btnKryterium_MouseLeave);
            this.btnKryterium5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnKryterium_MouseMove);
            // 
            // btnKryterium4
            // 
            this.btnKryterium4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKryterium4.AutoSize = true;
            this.btnKryterium4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnKryterium4.Depth = 0;
            this.btnKryterium4.Icon = null;
            this.btnKryterium4.Location = new System.Drawing.Point(1313, 268);
            this.btnKryterium4.Margin = new System.Windows.Forms.Padding(4);
            this.btnKryterium4.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnKryterium4.Name = "btnKryterium4";
            this.btnKryterium4.Primary = true;
            this.btnKryterium4.Size = new System.Drawing.Size(284, 36);
            this.btnKryterium4.TabIndex = 7;
            this.btnKryterium4.Text = "KRYTERIUM LAPLACE\'A";
            this.btnKryterium4.UseVisualStyleBackColor = true;
            this.btnKryterium4.Click += new System.EventHandler(this.btnKryteriumLaplacea);
            this.btnKryterium4.MouseEnter += new System.EventHandler(this.btnKryterium_MouseEnter);
            this.btnKryterium4.MouseLeave += new System.EventHandler(this.btnKryterium_MouseLeave);
            this.btnKryterium4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnKryterium_MouseMove);
            // 
            // btnKryterium3
            // 
            this.btnKryterium3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKryterium3.AutoSize = true;
            this.btnKryterium3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnKryterium3.Depth = 0;
            this.btnKryterium3.Icon = null;
            this.btnKryterium3.Location = new System.Drawing.Point(1311, 181);
            this.btnKryterium3.Margin = new System.Windows.Forms.Padding(4);
            this.btnKryterium3.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnKryterium3.Name = "btnKryterium3";
            this.btnKryterium3.Primary = true;
            this.btnKryterium3.Size = new System.Drawing.Size(274, 36);
            this.btnKryterium3.TabIndex = 6;
            this.btnKryterium3.Text = "KRYTERIUM SAVAGE\'A";
            this.btnKryterium3.UseVisualStyleBackColor = true;
            this.btnKryterium3.Click += new System.EventHandler(this.btnKryteriumSavagea);
            this.btnKryterium3.MouseEnter += new System.EventHandler(this.btnKryterium_MouseEnter);
            this.btnKryterium3.MouseLeave += new System.EventHandler(this.btnKryterium_MouseLeave);
            this.btnKryterium3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnKryterium_MouseMove);
            // 
            // btnKryterium2
            // 
            this.btnKryterium2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKryterium2.AutoSize = true;
            this.btnKryterium2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnKryterium2.Depth = 0;
            this.btnKryterium2.Icon = null;
            this.btnKryterium2.Location = new System.Drawing.Point(1304, 95);
            this.btnKryterium2.Margin = new System.Windows.Forms.Padding(4);
            this.btnKryterium2.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnKryterium2.Name = "btnKryterium2";
            this.btnKryterium2.Primary = true;
            this.btnKryterium2.Size = new System.Drawing.Size(244, 36);
            this.btnKryterium2.TabIndex = 5;
            this.btnKryterium2.Text = "KRYTERIUM WALDA";
            this.btnKryterium2.UseVisualStyleBackColor = true;
            this.btnKryterium2.Click += new System.EventHandler(this.btnKryteriumWalda);
            this.btnKryterium2.MouseEnter += new System.EventHandler(this.btnKryterium_MouseEnter);
            this.btnKryterium2.MouseLeave += new System.EventHandler(this.btnKryterium_MouseLeave);
            this.btnKryterium2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnKryterium_MouseMove);
            // 
            // btnKryterium1
            // 
            this.btnKryterium1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKryterium1.AutoSize = true;
            this.btnKryterium1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnKryterium1.Depth = 0;
            this.btnKryterium1.Icon = null;
            this.btnKryterium1.Location = new System.Drawing.Point(1313, 7);
            this.btnKryterium1.Margin = new System.Windows.Forms.Padding(4);
            this.btnKryterium1.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnKryterium1.Name = "btnKryterium1";
            this.btnKryterium1.Primary = true;
            this.btnKryterium1.Size = new System.Drawing.Size(284, 36);
            this.btnKryterium1.TabIndex = 4;
            this.btnKryterium1.Text = "KRYTERIUM HURWICZA";
            this.btnKryterium1.UseVisualStyleBackColor = true;
            this.btnKryterium1.Click += new System.EventHandler(this.btnKryteriumHurwicza);
            this.btnKryterium1.MouseEnter += new System.EventHandler(this.btnKryterium_MouseEnter);
            this.btnKryterium1.MouseLeave += new System.EventHandler(this.btnKryterium_MouseLeave);
            this.btnKryterium1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnKryterium_MouseMove);
            // 
            // materialTabSelector1
            // 
            this.materialTabSelector1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialTabSelector1.BaseTabControl = this.tabControl;
            this.materialTabSelector1.Depth = 0;
            this.materialTabSelector1.Enabled = false;
            this.materialTabSelector1.Location = new System.Drawing.Point(0, 117);
            this.materialTabSelector1.Margin = new System.Windows.Forms.Padding(4);
            this.materialTabSelector1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector1.Name = "materialTabSelector1";
            this.materialTabSelector1.Size = new System.Drawing.Size(1800, 72);
            this.materialTabSelector1.TabIndex = 1;
            this.materialTabSelector1.Text = "materialTabSelector1";
            // 
            // btnDalej
            // 
            this.btnDalej.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDalej.AutoSize = true;
            this.btnDalej.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDalej.Depth = 0;
            this.btnDalej.Icon = null;
            this.btnDalej.Location = new System.Drawing.Point(1424, 968);
            this.btnDalej.Margin = new System.Windows.Forms.Padding(4);
            this.btnDalej.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnDalej.Name = "btnDalej";
            this.btnDalej.Primary = true;
            this.btnDalej.Size = new System.Drawing.Size(96, 36);
            this.btnDalej.TabIndex = 2;
            this.btnDalej.Text = "DALEJ";
            this.btnDalej.UseVisualStyleBackColor = true;
            this.btnDalej.Click += new System.EventHandler(this.btnDalej_Click);
            // 
            // btnWstecz
            // 
            this.btnWstecz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWstecz.AutoSize = true;
            this.btnWstecz.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnWstecz.Depth = 0;
            this.btnWstecz.Icon = null;
            this.btnWstecz.Location = new System.Drawing.Point(10, 967);
            this.btnWstecz.Margin = new System.Windows.Forms.Padding(4);
            this.btnWstecz.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnWstecz.Name = "btnWstecz";
            this.btnWstecz.Primary = true;
            this.btnWstecz.Size = new System.Drawing.Size(118, 36);
            this.btnWstecz.TabIndex = 3;
            this.btnWstecz.Text = "WSTECZ";
            this.btnWstecz.UseVisualStyleBackColor = true;
            this.btnWstecz.Visible = false;
            this.btnWstecz.Click += new System.EventHandler(this.btnWstecz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1800, 1100);
            this.Controls.Add(this.btnWstecz);
            this.Controls.Add(this.btnDalej);
            this.Controls.Add(this.materialTabSelector1);
            this.Controls.Add(this.tabControl);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1800, 1100);
            this.Name = "Form1";
            this.Text = "Ocena Alternatyw";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTablicaStrat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTablicaWyplat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector1;
        private MaterialSkin.Controls.MaterialRaisedButton btnDodajDecyzje;
        private MaterialSkin.Controls.MaterialListView listViewDecyzje;
        private MaterialSkin.Controls.MaterialSingleLineTextField textBoxDecyzje;
        private System.Windows.Forms.ColumnHeader columnHeader;
        private MaterialSkin.Controls.MaterialRaisedButton btnDalej;
        private MaterialSkin.Controls.MaterialRaisedButton btnWstecz;
        private MaterialSkin.Controls.MaterialRaisedButton btnDodajStany;
        private MaterialSkin.Controls.MaterialListView listViewStany;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private MaterialSkin.Controls.MaterialSingleLineTextField textBoxStany;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataGridViewTablicaWyplat;
        private MaterialSkin.Controls.MaterialFlatButton btnWyczyscDecyzje;
        private MaterialSkin.Controls.MaterialFlatButton btnUsunDecyzje;
        private MaterialSkin.Controls.MaterialFlatButton btnUsunStany;
        private MaterialSkin.Controls.MaterialFlatButton btnWyczyscStany;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panelTrackBars;
        private MaterialSkin.Controls.MaterialRaisedButton btnKryterium1;
        private MaterialSkin.Controls.MaterialRaisedButton btnKryterium2;
        private MaterialSkin.Controls.MaterialRaisedButton btnKryterium3;
        private MaterialSkin.Controls.MaterialRaisedButton btnKryterium6;
        private MaterialSkin.Controls.MaterialRaisedButton btnKryterium5;
        private MaterialSkin.Controls.MaterialRaisedButton btnKryterium4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridViewTablicaStrat;
        private System.Windows.Forms.Label labelTablicaStrat;
        private System.Windows.Forms.Label labelTablicaWypłat;
        private System.Windows.Forms.Label labelDecyzjeZdominowane;
        private System.Windows.Forms.Label labelOdi;
        private System.Windows.Forms.Label labelOwdi;
        private MaterialSkin.Controls.MaterialRaisedButton btnKryterium7;
        private System.Windows.Forms.Label labelHint;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label labelCenaRabatowa;
        private System.Windows.Forms.Label labelCenaRegularna;
        private System.Windows.Forms.Label labelCena3;
        private System.Windows.Forms.Label labelCena2;
        private System.Windows.Forms.Label labelCena1;
        private MaterialSkin.Controls.MaterialSingleLineTextField textBoxCenaRabatowa;
        private MaterialSkin.Controls.MaterialSingleLineTextField textBoxCenaRegularna;
        private MaterialSkin.Controls.MaterialSingleLineTextField textBoxCena3;
        private MaterialSkin.Controls.MaterialSingleLineTextField textBoxCena2;
        private MaterialSkin.Controls.MaterialSingleLineTextField textBoxCena1;
    }
}

